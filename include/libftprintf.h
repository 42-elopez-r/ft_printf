/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/27 19:29:35 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/28 20:06:40 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <stdarg.h>

typedef struct	s_fmt_flags
{
	int			flag_zero;
	int			flag_left;

	int			width;
	int			precision;

	int			length_h;
	int			length_hh;
	int			length_l;
	int			length_ll;

	char		type;
}				t_fmt_flags;

void			ft_putunbr_base(unsigned long nbr, char *base);
int				ft_countunbr_base(unsigned long nbr, char *base);
void			add_filling(char char_fill, int size, int *len);
int				count_numbers(const char *str);
int				count_digits_unsig(unsigned int n);
int				count_digits_sig(int n, int include_sign);
int				fill_size_sing_int(t_fmt_flags *flags, int number);
int				parse_flags(const char *format, t_fmt_flags *flags);
int				parse_width(const char *format, va_list ap,
					t_fmt_flags *flags);
int				parse_precision(const char *format, va_list ap,
					t_fmt_flags *flags);
int				parse_length(const char *format, t_fmt_flags *flags);
int				parse_type(const char *format, t_fmt_flags *flags);
void			process_fmt_flags(int *len, va_list ap,
					t_fmt_flags *flags);
void			process_char(int *len, va_list ap, t_fmt_flags *flags);
void			process_string(int *len, va_list ap, t_fmt_flags *flags);
void			process_pointer(int *len, va_list ap, t_fmt_flags *flags);
void			process_signed_decimal(int *len, va_list ap,
					t_fmt_flags *flags);
void			process_unsigned_decimal(int *len, va_list ap,
					t_fmt_flags *flags);
void			process_unsigned_hex_low(int *len, va_list ap,
					t_fmt_flags *flags);
void			process_unsigned_hex_up(int *len, va_list ap,
					t_fmt_flags *flags);
void			process_percentage(int *len, t_fmt_flags *flags);
int				ft_printf(const char *format, ...);

#endif
