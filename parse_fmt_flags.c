/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_fmt_flags.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/16 20:03:02 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/28 20:06:29 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdarg.h>

/*
** This function takes the formatting string starting in the % char of a
** mark and the struct of formatting flags, modiying it acording to the flags
** found in the formatting string.
**
** Returns the amount of formatting chars processed.
*/

int		parse_flags(const char *format, t_fmt_flags *flags)
{
	int		chars_proc;

	format++;
	chars_proc = 1;
	while (*format == '-' || *format == '0')
	{
		if (*format == '-')
			flags->flag_left = 1;
		else
			flags->flag_zero = 1;
		chars_proc++;
		format++;
	}
	return (chars_proc);
}

/*
** This function takes the formatting string after the flags of a mark, the
** arguments list of ft_printf and the struct of formatting flags, modiying
** it acording to the width found in the formatting string.
**
** Returns the amount of formatting chars processed.
*/

int		parse_width(const char *format, va_list ap, t_fmt_flags *flags)
{
	if (!*format)
		return (0);
	else if (*format == '*')
	{
		flags->width = va_arg(ap, int);
		if (flags->width < 0)
		{
			flags->width *= -1;
			flags->flag_left = 1;
		}
		return (1);
	}
	else
	{
		flags->width = ft_atoi(format);
		return (count_numbers(format));
	}
}

/*
** This function takes the formatting string after the width of a mark, the
** arguments list of ft_printf and the struct of formatting flags, modiying
** it acording to the precision found in the formatting string.
**
** Returns the amount of formatting chars processed.
*/

int		parse_precision(const char *format, va_list ap, t_fmt_flags *flags)
{
	if (!*format)
		return (0);
	else if (!(*format++ == '.'))
		return (0);
	else if (*format == '*')
	{
		flags->precision = va_arg(ap, int);
		if (flags->precision < 0)
			flags->precision = -1;
		return (2);
	}
	else if (!ft_isdigit(*format))
	{
		flags->precision = 0;
		return (1);
	}
	else
	{
		flags->precision = ft_atoi(format);
		return (count_numbers(format) + 1);
	}
}

/*
** This function takes the formatting string after the precision of a mark
** and the struct of formatting flags, modiying
** it acording to the length found in the formatting string.
**
** Returns the amount of formatting chars processed.
*/

int		parse_length(const char *format, t_fmt_flags *flags)
{
	if (!*format)
		return (0);
	else if (*format == 'h' && format[1] != 'h')
	{
		flags->length_h = 1;
		return (1);
	}
	else if (*format == 'h' && format[1] == 'h')
	{
		flags->length_hh = 1;
		return (2);
	}
	else if (*format == 'l' && format[1] != 'l')
	{
		flags->length_l = 1;
		return (1);
	}
	else if (*format == 'l' && format[1] == 'l')
	{
		flags->length_ll = 1;
		return (2);
	}
	return (0);
}

/*
** This function takes the formatting string after the precision of a
** mark and the struct of formatting flags, modiying it acording to the type
** found in the formatting string.
**
** Returns the amount of formatting chars processed.
*/

int		parse_type(const char *format, t_fmt_flags *flags)
{
	if (!*format)
		return (0);
	else if (ft_strchr("cspdiuxX%", *format))
	{
		flags->type = *format;
		return (1);
	}
	return (0);
}
