/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/27 19:38:28 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/26 17:11:50 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdlib.h>

/*
** Displays a filling of char_fill, of the size specified and updates len
*/

void		add_filling(char char_fill, int size, int *len)
{
	int i;

	i = 0;
	while (i < size)
	{
		ft_putchar_fd(char_fill, 1);
		(*len)++;
		i++;
	}
}

/*
** This function counts the numbers at the left of a string
*/

int			count_numbers(const char *str)
{
	int n;

	n = 0;
	while (ft_isdigit(str[n]))
		n++;
	return (n);
}

/*
** Return the number of digits of an unsigned int
*/

int			count_digits_unsig(unsigned int n)
{
	int digits;

	if (n == 0)
		return (1);
	digits = 0;
	while (n > 0)
	{
		n /= 10;
		digits++;
	}
	return (digits);
}

/*
** Return the number of digits of an signed int, specifying if it should
** count the sign
*/

int			count_digits_sig(int n, int include_sign)
{
	int		digits;
	long	ln;

	if (n == 0)
		return (1);
	digits = 0;
	ln = n;
	if (ln < 0)
	{
		if (include_sign)
			digits++;
		ln *= -1;
	}
	while (ln > 0)
	{
		ln /= 10;
		digits++;
	}
	return (digits);
}

/*
** This function, sculpted little by little, test by test, calculates
** the size of the width in a signed integer conversion, armonized with
** the precision crazyness.
*/

int			fill_size_sing_int(t_fmt_flags *flags, int number)
{
	int precision_correction;
	int digits_sig;
	int digits_unsig;

	digits_sig = number == 0 && flags->precision == 0 ? 0 :
		count_digits_sig(number, 1);
	digits_unsig = count_digits_sig(number, 0);
	if (flags->precision != -1 && flags->precision > digits_sig)
		precision_correction = flags->precision - digits_unsig;
	else
		precision_correction = 0;
	return (flags->width - digits_sig - precision_correction);
}
