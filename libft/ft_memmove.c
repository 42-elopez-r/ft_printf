/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 14:30:46 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/04 17:13:22 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char	*b_dest;
	unsigned char	*b_src;
	size_t			i;

	if (!dest && !src)
		return (NULL);
	b_dest = (unsigned char*)dest;
	b_src = (unsigned char*)src;
	if (b_dest > b_src && b_dest <= b_src + n)
	{
		while (n--)
			b_dest[n] = b_src[n];
	}
	else
	{
		i = 0;
		while (i < n)
		{
			b_dest[i] = b_src[i];
			i++;
		}
	}
	return (dest);
}
