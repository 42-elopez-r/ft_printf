/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 16:55:21 by elopez-r          #+#    #+#             */
/*   Updated: 2019/11/27 18:37:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list *n_lst;
	t_list *aux;

	if (!lst || !(n_lst = malloc(sizeof(t_list))))
		return (NULL);
	while (lst)
	{
		ft_lstlast(n_lst)->content = f(lst->content);
		if (lst->next)
		{
			if ((aux = malloc(sizeof(t_list))))
				ft_lstadd_back(&n_lst, aux);
			else
			{
				ft_lstlast(n_lst)->next = NULL;
				ft_lstclear(&n_lst, del);
				return (NULL);
			}
		}
		else
			ft_lstlast(n_lst)->next = NULL;
		lst = lst->next;
	}
	return (n_lst);
}
