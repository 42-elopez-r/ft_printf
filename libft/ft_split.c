/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/13 13:44:17 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/02 17:21:03 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	count_words(char const *s, char c)
{
	int words;
	int i;

	i = 0;
	while (s[i] && s[i] == c)
		i++;
	words = ft_strlen(s + i) ? 1 : 0;
	while (s[i])
	{
		if (s[i] == c && s[i + 1] && s[i + 1] != c)
			words++;
		i++;
	}
	return (words);
}

static int	len_first_word(char const *s, char c)
{
	int i;
	int chars;

	chars = 0;
	i = 0;
	while (s[i] && s[i] == c)
		i++;
	while (s[i] && s[i] != c)
	{
		chars++;
		i++;
	}
	return (chars);
}

static int	fill_word(char *word, char const *s, char c)
{
	int i;
	int j;

	i = 0;
	while (s[i] && s[i] == c)
		i++;
	j = 0;
	while (s[i] && s[i] != c)
		word[j++] = s[i++];
	word[j] = '\0';
	while (s[i] && s[i] == c)
		i++;
	return (i);
}

void		emergency_clean(char **array, int arr_i)
{
	while (--arr_i >= 0)
		free(array[arr_i]);
	free(array);
}

char		**ft_split(char const *s, char c)
{
	char	**array;
	int		arr_i;
	int		word_len;

	array = NULL;
	if (s && (array = malloc(sizeof(char*) * (count_words(s, c) + 1))))
	{
		arr_i = 0;
		if (count_words(s, c))
		{
			while (*s)
			{
				word_len = len_first_word(s, c) + 1;
				if ((array[arr_i] = malloc(word_len)))
					s += fill_word(array[arr_i++], s, c);
				else
				{
					emergency_clean(array, arr_i);
					return (NULL);
				}
			}
		}
		array[arr_i] = NULL;
	}
	return (array);
}
