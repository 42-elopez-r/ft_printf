/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/08 11:02:39 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/03 16:39:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	*ft_calloc(size_t nmemb, size_t size)
{
	size_t	s_allocated;
	void	*allocated;

	if (!nmemb || !size)
	{
		nmemb = 1;
		size = 1;
	}
	s_allocated = nmemb * size;
	if (s_allocated <= 0)
		return (NULL);
	if ((allocated = malloc(s_allocated)))
	{
		ft_bzero(allocated, s_allocated);
		return (allocated);
	}
	else
		return (NULL);
}
