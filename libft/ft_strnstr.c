/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/06 20:44:10 by elopez-r          #+#    #+#             */
/*   Updated: 2019/12/03 16:29:21 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

static int	same_start(const char *s1, const char *s2, size_t len)
{
	size_t i;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] != s2[i])
			return (0);
		i++;
	}
	if (s1[i])
		return (0);
	return (i <= len);
}

char		*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t i;

	if (!*little)
		return ((char*)big);
	i = 0;
	while (big[i] && i < len)
	{
		if (same_start(little, big + i, len - i))
			return ((char*)big + i);
		i++;
	}
	return (NULL);
}
