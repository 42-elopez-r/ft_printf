/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putunbr_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/14 17:55:36 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/27 15:43:26 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <unistd.h>

static void		display_number(unsigned long nbr, char *base, int base_n)
{
	if (nbr > 0)
	{
		display_number(nbr / base_n, base, base_n);
		write(1, base + nbr % base_n, 1);
	}
}

static void		count_number(int *chars, unsigned long nbr, char *base,
		int base_n)
{
	if (nbr > 0)
	{
		count_number(chars, nbr / base_n, base, base_n);
		(*chars)++;
	}
}

void			ft_putunbr_base(unsigned long nbr, char *base)
{
	if (nbr == 0)
		write(1, base, 1);
	display_number(nbr, base, ft_strlen(base));
}

int				ft_countunbr_base(unsigned long nbr, char *base)
{
	int n;

	if (nbr == 0)
		return (1);
	n = 0;
	count_number(&n, nbr, base, ft_strlen(base));
	return (n);
}
