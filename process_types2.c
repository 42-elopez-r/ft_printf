/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_types2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 19:28:22 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/27 14:03:10 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdarg.h>

static void	process_unsigned_hex(int *len, va_list ap, t_fmt_flags *flags,
		char *base)
{
	unsigned int	number;
	int				filling_size;
	int				digs_number;

	number = va_arg(ap, unsigned int);
	digs_number = number == 0 && flags->precision == 0 ? 0 :
		ft_countunbr_base(number, base);
	filling_size = flags->width - digs_number - (flags->precision != -1 &&
			flags->precision > digs_number ?
			flags->precision - digs_number : 0);
	if (!flags->flag_left)
		add_filling(flags->flag_zero && flags->precision == -1 ? '0' : ' ',
				filling_size, len);
	if (flags->precision != -1)
		add_filling('0', flags->precision - digs_number, len);
	if (!(number == 0 && flags->precision == 0))
	{
		ft_putunbr_base(number, base);
		(*len) += digs_number;
	}
	if (flags->flag_left)
		add_filling(' ', filling_size, len);
}

void		process_unsigned_hex_low(int *len, va_list ap, t_fmt_flags *flags)
{
	process_unsigned_hex(len, ap, flags, "0123456789abcdef");
}

void		process_unsigned_hex_up(int *len, va_list ap, t_fmt_flags *flags)
{
	process_unsigned_hex(len, ap, flags, "0123456789ABCDEF");
}

void		process_percentage(int *len, t_fmt_flags *flags)
{
	if (!flags->flag_left)
		add_filling(flags->flag_zero ? '0' : ' ', flags->width - 1, len);
	ft_putchar_fd('%', 1);
	(*len)++;
	if (flags->flag_left)
		add_filling(' ', flags->width - 1, len);
}
