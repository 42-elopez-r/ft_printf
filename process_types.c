/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_types.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/19 19:56:56 by elopez-r          #+#    #+#             */
/*   Updated: 2020/01/27 17:02:33 by elopez-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"
#include "libft.h"
#include <stdarg.h>

/*
** Displays the expansion of a char mark and updates then length of the
** outputted string
*/

void	process_char(int *len, va_list ap, t_fmt_flags *flags)
{
	if (flags->flag_left)
		ft_putchar_fd((unsigned char)va_arg(ap, int), 1);
	add_filling(' ', flags->width - 1, len);
	if (!flags->flag_left)
		ft_putchar_fd((unsigned char)va_arg(ap, int), 1);
	(*len)++;
}

/*
** Displays the expansion of a string mark and updates then length of the
** outputted string
*/

void	process_string(int *len, va_list ap, t_fmt_flags *flags)
{
	int		chars_displayed;
	char	*string;
	int		filling_size;

	chars_displayed = 0;
	string = va_arg(ap, char*);
	if (!string)
		string = "(null)";
	filling_size = flags->width - (ft_strlen(string) >
			(size_t)flags->precision ?
			flags->precision : ft_strlen(string));
	if (!flags->flag_left)
		add_filling(flags->flag_zero ? '0' : ' ', filling_size, len);
	while (*string &&
			(chars_displayed < flags->precision || flags->precision == -1))
	{
		ft_putchar_fd(*string++, 1);
		(*len)++;
		chars_displayed++;
	}
	if (flags->flag_left)
		add_filling(' ', filling_size, len);
}

/*
** Displays the expansion of a unsigned int mark and updates then length of the
** outputted string
*/

void	process_unsigned_decimal(int *len, va_list ap, t_fmt_flags *flags)
{
	unsigned int	number;
	int				filling_size;
	int				digs_number;

	number = va_arg(ap, unsigned int);
	digs_number = number == 0 && flags->precision == 0 ? 0 :
		count_digits_unsig(number);
	filling_size = flags->width - digs_number - (flags->precision != -1 &&
			flags->precision > digs_number ?
			flags->precision - digs_number : 0);
	if (!flags->flag_left)
		add_filling(flags->flag_zero && flags->precision == -1 ? '0' : ' ',
				filling_size, len);
	if (flags->precision != -1)
		add_filling('0', flags->precision - digs_number, len);
	if (!(number == 0 && flags->precision == 0))
	{
		ft_putunbr_fd(number, 1);
		(*len) += digs_number;
	}
	if (flags->flag_left)
		add_filling(' ', filling_size, len);
}

void	process_signed_decimal(int *len, va_list ap, t_fmt_flags *flags)
{
	int	number;
	int	filling_size;
	int	digs_nbr;

	number = va_arg(ap, int);
	digs_nbr = number == 0 && flags->precision == 0 ? 0 :
		count_digits_sig(number, 1);
	filling_size = fill_size_sing_int(flags, number);
	if (flags->flag_zero && flags->precision == -1 && number < 0)
		ft_putchar_fd('-', 1);
	if (!flags->flag_left)
		add_filling(flags->flag_zero && flags->precision == -1 ? '0' : ' ',
				filling_size, len);
	if (!(flags->flag_zero && flags->precision == -1) && number < 0)
		ft_putchar_fd('-', 1);
	if (flags->precision != -1)
		add_filling('0', flags->precision - count_digits_sig(number, 0), len);
	if (!(number == 0 && flags->precision == 0))
	{
		ft_putnbr_ns_fd(number, 1);
		(*len) += digs_nbr;
	}
	if (flags->flag_left)
		add_filling(' ', filling_size, len);
}

void	process_pointer(int *len, va_list ap, t_fmt_flags *flags)
{
	unsigned long	number;
	int				filling_size;
	int				digs_number;

	number = va_arg(ap, unsigned long);
	digs_number = 2 + (number == 0 && flags->precision == 0 ? 0 :
		ft_countunbr_base(number, "0123456789abcdef"));
	filling_size = flags->width - digs_number - (flags->precision != -1 &&
			flags->precision > digs_number ?
			flags->precision - digs_number : 0);
	if (!flags->flag_left)
	{
		ft_putstr_fd(flags->flag_zero ? "0x" : "", 1);
		add_filling(flags->flag_zero && flags->precision == -1 ? '0' : ' ',
				filling_size, len);
	}
	ft_putstr_fd(!flags->flag_zero ? "0x" : "", 1);
	if (flags->precision != -1)
		add_filling('0', flags->precision - digs_number + 2, len);
	if (!(number == 0 && flags->precision == 0))
		ft_putunbr_base(number, "0123456789abcdef");
	(*len) += digs_number;
	if (flags->flag_left)
		add_filling(' ', filling_size, len);
}
