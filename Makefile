# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: elopez-r <elopez-r@student.42madrid>       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/10/20 15:16:31 by elopez-r          #+#    #+#              #
#    Updated: 2020/01/27 15:52:17 by elopez-r         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = cc
CFLAGS += -Wall -Wextra -Werror -Iinclude
AR = ar

NAME = libftprintf.a

HEADER = include/libftprintf.h
LIBFT_HEADER = include/libft.h

SRC_LIBFT=	libft/ft_atoi.c \
			libft/ft_bzero.c \
			libft/ft_calloc.c \
			libft/ft_isalnum.c \
			libft/ft_isalpha.c \
			libft/ft_isascii.c \
			libft/ft_isdigit.c \
			libft/ft_isprint.c \
			libft/ft_itoa.c \
			libft/ft_lstadd_back_bonus.c \
			libft/ft_lstadd_front_bonus.c \
			libft/ft_lstclear_bonus.c \
			libft/ft_lstdelone_bonus.c \
			libft/ft_lstiter_bonus.c \
			libft/ft_lstlast_bonus.c \
			libft/ft_lstmap_bonus.c \
			libft/ft_lstnew_bonus.c \
			libft/ft_lstsize_bonus.c \
			libft/ft_memccpy.c \
			libft/ft_memchr.c \
			libft/ft_memcmp.c \
			libft/ft_memcpy.c \
			libft/ft_memmove.c \
			libft/ft_memset.c \
			libft/ft_putchar_fd.c \
			libft/ft_putendl_fd.c \
			libft/ft_putnbr_fd.c \
			libft/ft_putstr_fd.c \
			libft/ft_split.c \
			libft/ft_strchr.c \
			libft/ft_strdup.c \
			libft/ft_strjoin.c \
			libft/ft_strlcat.c \
			libft/ft_strlcpy.c \
			libft/ft_strlen.c \
			libft/ft_strmapi.c \
			libft/ft_strncmp.c \
			libft/ft_strnstr.c \
			libft/ft_strrchr.c \
			libft/ft_strtrim.c \
			libft/ft_substr.c \
			libft/ft_tolower.c \
			libft/ft_toupper.c

SRC		 =	 ft_printf.c \
			 utils.c \
			 parse_fmt_flags.c \
			 process_types.c \
			 process_types2.c \
			 ft_putunbr_base.c

OBJS = $(SRC:.c=.o)
OBJS_LIBFT = $(SRC_LIBFT:.c=.o)

all: $(NAME)

$(NAME): libft $(OBJS) $(HEADER) $(LIBFT_HEADER)
	$(AR) -rcs $(NAME) $(OBJS)

libft: $(OBJS_LIBFT) $(LIBFT_HEADER)
	$(AR) -rcs $(NAME) $(OBJS_LIBFT)

debug: CFLAGS += -g
debug: fclean all
	$(CC) $(CFLAGS) test.c $(NAME) -o test

.o: .c
	$(CC) $(CFLAGS) -c $< -o $@

bonus: all

clean:
	rm -f $(OBJS)
	rm -f $(OBJS_LIBFT)

fclean: clean
	rm -rf $(NAME) test test.dSYM

re: fclean all

.PHONY: all clean fclean re bonus debug libft
